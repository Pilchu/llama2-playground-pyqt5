import json
import sys
from datetime import timedelta

import requests
from PyQt5.QtCore import Qt, QThread, QTimer, pyqtSignal
from PyQt5.QtGui import (QColor, QIcon, QPalette, QStandardItem,
                         QStandardItemModel)
from PyQt5.QtWidgets import (QApplication, QComboBox, QDockWidget, QFileDialog,
                             QHBoxLayout, QInputDialog, QLabel, QListWidget,
                             QMainWindow, QPushButton, QSizePolicy, QSlider,
                             QSpacerItem, QSplitter, QStyledItemDelegate,
                             QTableView, QTextEdit, QTreeView, QVBoxLayout,
                             QWidget)


class TextEditDelegate(QStyledItemDelegate):
    def createEditor(self, parent, option, index):
        editor = CustomTextEdit(parent)
        return editor

    def setModelData(self, editor, model, index):
        plain_text = editor.toPlainText()
        model.setData(index, plain_text, Qt.EditRole)


class CustomTextEdit(QTextEdit):
    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Enter or event.key() == Qt.Key_Return:
            if self.textCursor().hasSelection():
                self.textCursor().removeSelectedText()
            self.textCursor().insertText("\n")
        else:
            super().keyPressEvent(event)


class ComboBoxDelegate(QStyledItemDelegate):
    def __init__(self, parent=None):
        super().__init__(parent)

    def createEditor(self, parent, option, index):
        editor = QComboBox(parent)
        editor.addItem("user")
        editor.addItem("assistant")
        editor.setEditable(False)
        return editor

    def setEditorData(self, editor, index):
        text = index.model().data(index, Qt.EditRole)
        idx = editor.findText(text)
        if idx >= 0:
            editor.setCurrentIndex(idx)

    def setModelData(self, editor, model, index):
        model.setData(index, editor.currentText(), Qt.EditRole)


class ChatModelThread(QThread):
    response_received = pyqtSignal(str)
    response_finished = pyqtSignal()
    response_error = pyqtSignal()

    def __init__(
        self,
        messages,
        temperature,
        token_limit,
        stop,
        top_p,
        frequency_penality,
        presence_penality,
    ):
        super().__init__()
        self.messages = messages
        self.temperature = temperature
        self.token_limit = token_limit
        self.stop = stop
        self.top_p = top_p
        self.frequency_penality = frequency_penality
        self.presence_penality = presence_penality
        self._is_running = True

    def stop_listening(self):
        self._is_running = False

    def run(self):
        url = "http://localhost:8080/llama2/chat"

        payload = {
            "messages": self.messages,
            "temperature": self.temperature,
            "top_p": self.top_p,
            "stop": self.stop,
            "max_tokens": self.token_limit,
            "presence_penalty": self.presence_penality,
            "frequency_penalty": self.frequency_penality,
            "stream": True,
        }
        headers = {"Content-Type": "application/json"}

        with requests.post(
            url, json=payload, headers=headers, verify=False, stream=True
        ) as r:
            for line in r.iter_lines():
                if not self._is_running:
                    break

                if line:
                    decoded_line = line.decode("utf-8")
                    try:
                        resp = json.loads(decoded_line)
                        self.response_received.emit(
                            resp["choices"][0]["delta"].get("content", "")
                        )
                    except json.decoder.JSONDecodeError as e:
                        self.response_error.emit()
                        return

        self.response_finished.emit()


class ChatTableView(QTableView):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.horizontalHeader().setStretchLastSection(True)
        self.verticalHeader().setVisible(False)
        self.setWordWrap(True)
        self.setTextElideMode(Qt.ElideNone)

    def resizeEvent(self, event):
        self.resizeRowsToContents()
        super().resizeEvent(event)


class ChatWindow(QMainWindow):
    palette = QApplication.palette()
    user_color = palette.color(QPalette.AlternateBase)
    assistant_color = palette.color(QPalette.Base)
    in_progress_color = QColor(165, 235, 235)

    def __init__(self):
        super().__init__()

        self.setWindowIcon(QIcon("icon.png"))

        self.chat_thread = None
        self.streamed_message_item = None

        self.main_layout = QVBoxLayout()

        self.splitter = QSplitter(Qt.Vertical)

        self.conversation_model = QStandardItemModel()
        self.conversation_model.setHorizontalHeaderLabels(["Role", "Content"])

        self.conversation_view = ChatTableView(self)
        self.conversation_view.setModel(self.conversation_model)
        self.conversation_view.setEditTriggers(QTreeView.DoubleClicked)

        self.delegate = ComboBoxDelegate(self.conversation_view)
        self.conversation_view.setItemDelegateForColumn(0, self.delegate)
        self.conversation_view.setItemDelegateForColumn(1, TextEditDelegate())

        self.system_message_input = QTextEdit()
        self.system_message_input.setPlaceholderText("Enter system message here...")

        self.message_input = QTextEdit()
        self.message_input.setPlaceholderText("Enter message content here...")

        self.splitter.addWidget(self.system_message_input)
        self.splitter.addWidget(self.conversation_view)
        self.splitter.addWidget(self.message_input)

        self.user_button = QPushButton("User")
        self.user_button.clicked.connect(lambda: self.add_message("user"))

        self.assistant_button = QPushButton("Assistant")
        self.assistant_button.clicked.connect(lambda: self.add_message("assistant"))

        self.generate_button = QPushButton("Generate")
        self.generate_button.clicked.connect(self.send_to_model)
        self.waiting_timer = QTimer(self)
        self.waiting_timer.timeout.connect(self.update_waiting_time)
        self.waiting_seconds = 0

        self.delete_message_button = QPushButton("Delete message")
        self.delete_message_button.clicked.connect(self.delete_selected_message)

        self.button_layout = QHBoxLayout()
        self.button_layout.addWidget(self.user_button)
        self.button_layout.addWidget(self.assistant_button)
        spacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.button_layout.addSpacerItem(spacer)
        self.button_layout.addWidget(self.delete_message_button)
        spacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.button_layout.addSpacerItem(spacer)
        self.button_layout.addWidget(self.generate_button)

        self.main_layout.addWidget(self.splitter)
        self.main_layout.addLayout(self.button_layout)

        self.central_widget = QWidget()
        self.central_widget.setLayout(self.main_layout)
        self.setCentralWidget(self.central_widget)

        self.conversation_model.itemChanged.connect(self.on_item_changed)

        self.parameters_dock = QDockWidget("Model parameters", self)
        self.parameters_widget = QWidget()
        self.parameters_layout = QVBoxLayout()

        self.temperature_label = QLabel("Temperature [0.20]")
        self.temperature_slider = QSlider(Qt.Horizontal)
        self.temperature_slider.setMinimum(0)
        self.temperature_slider.setMaximum(100)
        self.temperature_slider.setValue(20)
        self.temperature_slider.valueChanged.connect(self.update_temperature_label)
        self.parameters_layout.addWidget(self.temperature_label)
        self.parameters_layout.addWidget(self.temperature_slider)

        self.token_limit_label = QLabel("Max tokens [No Limit]")
        self.token_limit_slider = QSlider(Qt.Horizontal)
        self.token_limit_slider.setMinimum(0)
        self.token_limit_slider.setMaximum(4096)
        self.token_limit_slider.setValue(0)
        self.token_limit_slider.valueChanged.connect(self.update_token_limit_label)
        self.parameters_layout.addWidget(self.token_limit_label)
        self.parameters_layout.addWidget(self.token_limit_slider)

        self.stop_sequence_list = QListWidget()
        self.add_stop_sequence_button = QPushButton("Add")
        self.remove_stop_sequence_button = QPushButton("Remove")
        self.add_stop_sequence_button.clicked.connect(self.add_stop_sequence)
        self.remove_stop_sequence_button.clicked.connect(self.remove_stop_sequence)

        self.parameters_layout.addWidget(QLabel("Stop Sequences"))
        self.parameters_layout.addWidget(self.stop_sequence_list)
        self.parameters_layout.addWidget(self.add_stop_sequence_button)
        self.parameters_layout.addWidget(self.remove_stop_sequence_button)

        self.top_p_label = QLabel("Top P [0.95]")
        self.top_p_slider = QSlider(Qt.Horizontal)
        self.top_p_slider.setMinimum(0)
        self.top_p_slider.setMaximum(100)
        self.top_p_slider.setValue(95)
        self.top_p_slider.valueChanged.connect(self.update_top_p_label)

        self.parameters_layout.addWidget(self.top_p_label)
        self.parameters_layout.addWidget(self.top_p_slider)

        self.frequency_penalty_label = QLabel("Frequency Penalty [0.00]")
        self.frequency_penalty_slider = QSlider(Qt.Horizontal)
        self.frequency_penalty_slider.setMinimum(0)
        self.frequency_penalty_slider.setMaximum(200)
        self.frequency_penalty_slider.setValue(0)
        self.frequency_penalty_slider.valueChanged.connect(
            self.update_frequency_penalty_label
        )

        self.parameters_layout.addWidget(self.frequency_penalty_label)
        self.parameters_layout.addWidget(self.frequency_penalty_slider)

        self.presence_penalty_label = QLabel("Presence Penalty [0.00]")
        self.presence_penalty_slider = QSlider(Qt.Horizontal)
        self.presence_penalty_slider.setMinimum(0)
        self.presence_penalty_slider.setMaximum(200)
        self.presence_penalty_slider.setValue(0)
        self.presence_penalty_slider.valueChanged.connect(
            self.update_presence_penalty_label
        )

        self.parameters_layout.addWidget(self.presence_penalty_label)
        self.parameters_layout.addWidget(self.presence_penalty_slider)

        self.parameters_layout.addSpacerItem(
            QSpacerItem(20, 20, QSizePolicy.Minimum, QSizePolicy.Expanding)
        )

        self.save_conversation_button = QPushButton("Save conversation")
        self.load_conversation_button = QPushButton("Load conversation")

        self.parameters_layout.addWidget(self.save_conversation_button)
        self.parameters_layout.addWidget(self.load_conversation_button)
        self.save_conversation_button.clicked.connect(self.select_file_to_save)
        self.load_conversation_button.clicked.connect(self.select_file_to_load)

        self.parameters_widget.setLayout(self.parameters_layout)
        self.parameters_dock.setWidget(self.parameters_widget)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.parameters_dock)

        # Ustawienia okna
        self.setWindowTitle("LLamA2 Playground")
        self.resize(1200, 800)

    def update_temperature_label(self, value):
        self.temperature_label.setText(f"Temperature [{value / 100:.2f}]")

    def update_top_p_label(self, value):
        self.top_p_label.setText(f"Top P [{value / 100:.2f}]")

    def update_frequency_penalty_label(self, value):
        self.frequency_penalty_label.setText(f"Frequency Penalty [{value / 100:.2f}]")

    def update_presence_penalty_label(self, value):
        self.presence_penalty_label.setText(f"Presence Penalty [{value / 100:.2f}]")

    def update_token_limit_label(self, value):
        self.token_limit_label.setText(f"Max tokens [{value or 'No limit'}]")

    def add_stop_sequence(self):
        text, ok = QInputDialog.getText(self, "Add stop sequence", "Stop sequence:")
        if ok and text:
            self.stop_sequence_list.addItem(text)

    def remove_stop_sequence(self):
        list_items = self.stop_sequence_list.selectedItems()
        if not list_items:
            return
        for item in list_items:
            self.stop_sequence_list.takeItem(self.stop_sequence_list.row(item))

    def add_message(self, role):
        message = self.message_input.toPlainText().strip()
        if message:
            self.update_conversation_model(role, message)
            self.message_input.clear()

    def prepare_conversation_output(self):
        messages = [
            {"role": "system", "content": self.system_message_input.toPlainText()}
        ]
        for row in range(self.conversation_model.rowCount()):
            role = self.conversation_model.item(row, 0).text()
            content = self.conversation_model.item(row, 1).text()
            messages.append({"role": role, "content": content})
        return {
            "messages": messages,
            "temperature": self.temperature_slider.value() / 100,
            "max_tokens": self.token_limit_slider.value(),
            "stop": [
                self.stop_sequence_list.item(i).text()
                for i in range(self.stop_sequence_list.count())
            ],
            "top_p": self.top_p_slider.value() / 100,
            "frequency_penalty": self.frequency_penalty_slider.value() / 100,
            "presence_penalty": self.presence_penalty_slider.value() / 100,
        }

    def send_to_model(self):
        self.waiting_seconds = 0
        self.update_waiting_time()
        self.waiting_timer.start(1000)

        self.chat_thread = ChatModelThread(**self.prepare_conversation_output())
        self.generate_button.setText("Waiting...")
        self.generate_button.clicked.disconnect()
        self.generate_button.clicked.connect(self.chat_thread.stop_listening)

        role_item = QStandardItem("assistant")
        self.streamed_message_item = QStandardItem("")
        role_item.setBackground(self.assistant_color)
        self.streamed_message_item.setBackground(self.in_progress_color)
        self.conversation_model.appendRow([role_item, self.streamed_message_item])

        self.chat_thread.response_received.connect(self.update_response_stream)
        self.chat_thread.response_finished.connect(self.end_response_stream)
        self.chat_thread.response_error.connect(self.error_in_response_stream)
        self.chat_thread.start()

    @staticmethod
    def seconds_to_human_readable(seconds):
        # Create a timedelta object
        td = timedelta(seconds=seconds)

        # Extract hours, minutes, and seconds
        hours, remainder = divmod(td.seconds, 3600)
        minutes, seconds = divmod(remainder, 60)

        # Format the string
        time_parts = []
        if hours:
            time_parts.append(f"{hours} hr")
        if minutes:
            time_parts.append(f"{minutes} min")
        if seconds or not time_parts:
            time_parts.append(f"{seconds} sec")

        return " ".join(time_parts)

    def update_waiting_time(self):
        self.generate_button.setText(
            f"Stop [{self.seconds_to_human_readable(self.waiting_seconds)}]"
        )
        self.waiting_seconds += 1

    def update_response_stream(self, response):
        self.streamed_message_item.setText(self.streamed_message_item.text() + response)
        self.conversation_view.viewport().update()

    def error_in_response_stream(self):
        self.end_response_stream()
        self.streamed_message_item.setBackground(QColor(255, 100, 100))
        self.streamed_message_item.setText(
            self.streamed_message_item.text() + "[ERROR OCCURED]"
        )

    def end_response_stream(self):
        self.waiting_timer.stop()
        self.streamed_message_item.setBackground(self.assistant_color)
        self.generate_button.setText("Generate")
        self.generate_button.clicked.disconnect()
        self.generate_button.clicked.connect(self.send_to_model)

    def update_conversation_model(self, role, content):
        if content:
            role_item = QStandardItem(role)
            message_item = QStandardItem(content)

            if role == "user":
                color = self.user_color
            else:
                color = self.assistant_color

            role_item.setBackground(color)
            message_item.setBackground(color)

            self.conversation_model.appendRow([role_item, message_item])

            self.conversation_view.scrollToBottom()
            self.conversation_view.resizeRowsToContents()

    def delete_selected_message(self):
        selected_index = self.conversation_view.currentIndex()
        if selected_index.isValid():
            self.conversation_model.removeRow(selected_index.row())

    def on_item_changed(self, item):
        if item.column() == 0:
            row = item.row()
            role_item = self.conversation_model.item(row, 0)
            message_item = self.conversation_model.item(row, 1)

            if role_item.text() == "user":
                color = self.user_color
            else:
                color = self.assistant_color

            role_item.setBackground(color)
            message_item.setBackground(color)

        self.conversation_view.resizeRowsToContents()

    def select_file_to_save(self):
        options = QFileDialog.Options()
        path, _ = QFileDialog.getSaveFileName(
            self, "Save File", "", "JSON Files (*.json)", options=options
        )
        if path:
            self.save_file(path)

    def save_file(self, path):
        with open(path, "w") as output:
            output.write(json.dumps(self.prepare_conversation_output()))

    def select_file_to_load(self):
        options = QFileDialog.Options()
        path, _ = QFileDialog.getOpenFileName(
            self, "Open File", "", "JSON Files (*.json)", options=options
        )
        if path:
            self.load_file(path)

    def load_file(self, path):
        with open(path, "r") as input_file:
            data = json.load(input_file)

            self.conversation_model.clear()
            self.conversation_model.setHorizontalHeaderLabels(["Role", "Content"])
            self.system_message_input.clear()
            self.message_input.clear()

            self.system_message_input.setText(
                "\n".join(
                    msg["content"]
                    for msg in data["messages"]
                    if msg["role"] == "system"
                )
            )

            for msg in data["messages"]:
                if msg["role"] == "system":
                    continue
                self.update_conversation_model(**msg)

            self.temperature_slider.setValue(int(data.get("temperature", 0.2) * 100)),
            self.token_limit_slider.setValue(data.get("token_limit", 0)),
            self.stop_sequence_list.clear()
            for stop_seq in data.get("stop", []):
                self.stop_sequence_list.addItem(stop_seq)
            self.top_p_slider.setValue(int(data.get("top_p", 0.95) * 100)),
            self.frequency_penalty_slider.setValue(
                int(data.get("frequency_penality", 0) * 100)
            ),
            self.presence_penalty_slider.setValue(
                int(data.get("presence_penality", 0) * 100)
            ),


if __name__ == "__main__":
    app = QApplication(sys.argv)
    mainWin = ChatWindow()
    mainWin.show()
    sys.exit(app.exec_())
